from PIL import Image
import numpy as np
import tensorflow as tf
import os
import requests
import pandas as pd
from datetime import datetime as dt
import mysql.connector
import shutil
from pyproj import Proj, transform






def preprocess_image(img):
    img_size = (224, 224)
    img = img.resize(img_size)
    img = np.array(img) / 255.0
    img = np.expand_dims(img, axis=0)
    return img

def get_traffic_density():

    model = tf.keras.models.load_model('trafficnet_model.h5')
    # Check if the response was successful
    res = []
    in_proj = Proj("EPSG:2157")  # Irish Transverse Mercator (ITM)
    out_proj = Proj("EPSG:4326")  # WGS 84 (latitude and longitude)
    url = 'https://tiitrafficdata.azurewebsites.net/api/CCTV?_=1676563284495'
    response = requests.get(url)
    if response.status_code == 200:
        # Convert the response to a JSON object
        data = response.json()
        # Create a new directory to store the images
        if not os.path.exists('images'):
            os.makedirs('images')

        i=0
        # Download and classify each image
        for d in data["features"]:
            i+=1
            # Get the image URL and file name
            image_url = d['properties']['camera_image']
            filename = os.path.join('images', os.path.basename(str(i)))
            # Download the image and save it to the file
            image_data = requests.get(image_url).content
            with open(filename, 'wb') as f:
                f.write(image_data)
            # Load the image and preprocess it
            img = Image.open(filename)
            img = preprocess_image(img)
            # Use the model to predict the class label
            predictions = model.predict(img)
            predicted_class = np.argmax(predictions[0])
            if(predicted_class == 1):
                 
                x, y = d['geometry']['coordinates']
                latitude,longitude = transform(in_proj, out_proj, x, y)
                temp = [d['id']]
                temp.append(latitude)
                temp.append(longitude)
                temp.append(d['properties']['camera_image'])

                
                res.append(temp)

    return res


result = get_traffic_density()
shutil.rmtree('images')
cnx = mysql.connector.connect(user='root', password='rootpassword',
                                host='35.187.183.86', port = '3306',
                                database='ase')
cursor = cnx.cursor()

data = pd.DataFrame(result)

cursor.execute("select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_TRFC_CAMS\"")
result_set = cursor.fetchall()
# print(result_set[0][0])

for index, row in data.iterrows():
        insert_stmt = "INSERT INTO dev2_ase_scm.stg_irl_dub_trfc_cams (id,cycl_time_id,url,lat, lon) values(%s,%s,%s,%s,%s)"
        data = (row[0],result_set[0][0],row[3], row[1], row[2])
        cursor.execute(insert_stmt, data)


cursor.execute("TRUNCATE TABLE dev3_ase_scm.dim_irl_dub_trfc_cams")
cursor.execute("insert into dev3_ase_scm.dim_irl_dub_trfc_cams( id, cycl_time_id,url, lat, lon,inrt_ts ) select id, cycl_time_id,url, lat, lon,inrt_ts from dev2_ase_scm.stg_irl_dub_trfc_cams where cycl_time_id = ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_TRFC_CAMS\" )")

cursor.execute("with curr_cycl as ( select cycl_time_id from ase_scm_cntl.cntl_etl_sql where rqt_nm = \"ASE_DUB_TRFC_CAMS\" ) update ase_scm_cntl.cntl_etl_sql set lst_updt_cycl = (select cycl_time_id from curr_cycl) where rqt_nm = \"ASE_DUB_TRFC_CAMS\"")
cursor.execute("update ase_scm_cntl.cntl_etl_sql set cycl_time_id = concat( '1', year(sysdate()), lpad(month(sysdate()),2,0), lpad(day(sysdate()),2,0), lpad(hour(sysdate()),2,0), lpad(minute(sysdate()),2,0) ) where rqt_nm = \"ASE_DUB_TRFC_CAMS\"")

cnx.commit()
cursor.close()


import unittest
from traffic_density_model import get_traffic_density, preprocess_image
from PIL import Image

class TestTrafficDensityMethods(unittest.TestCase):

    def test_getTraffic_density(self):
        res = get_traffic_density()
        self.assertNotEqual(len(res),0)

    def test_preprocess_image(self):
        img = Image.open('hex.png')
        res = preprocess_image(img)
        self.assertNotEqual(len(res),0)


if __name__ == '__main__':
    unittest.main()
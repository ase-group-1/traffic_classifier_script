The traffic classifier fetches images of traffic cameras from the website "tiitraffic.ie" in real-time to monitor traffic in Dublin. 
The collected data is then processed using the ResNet-50 model to identify regions of traffic congestion.

To run the traffic classifier, we follow the following steps:

Install all the required libraries using the terminal inside the project directory


 `pip install -r requirements.txt`

`python traffic_density_model.py`

This script gets the camera images and stores the coordinates of the regions where traffic density is high in the database.
